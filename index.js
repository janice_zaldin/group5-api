import express from 'express';

import patientRoute from "./src/routes/Patients.js";
import CareProviders from "./src/routes/CareProviders.js"
import careProvidersAuthRoute from "./src/routes/authCare.js"
import authAdminRoute from "./src/routes/authAdmin.js"
import labResults from "./src/routes/labResults.js"
import patientImaging from "./src/routes/Imaging.js"
import patientBilling from "./src/routes/Billing.js"
import patientNotes from "./src/routes/PatientNotes.js"
import searchPatients from "./src/routes/SearchPatients.js"
import searchCareProviders from "./src/routes/CareProviders.js"
import AllergiesMedications from "./src/routes/allergiesMedications.js"


import cors from "cors";

const app = express();
app.use(cors());

//middleware
app.use(express.json());
const port = process.env.PORT || 3007;

app.get("/api", (req, res) => {
  res.send("hello world");
});

app.use("/api/Patients", patientRoute);
app.use("/api/CareProviders", CareProviders);
app.use("/api/authCare", careProvidersAuthRoute);
app.use("/api/authAdmin", authAdminRoute);
app.use("/api/labResults", labResults);
app.use("/api/imaging", patientImaging);
app.use("/api/Billing", patientBilling);
app.use("/api/PatientNotes", patientNotes);
app.use("/api/SearchPatients", searchPatients);
app.use("/api/SearchCareProviders", searchCareProviders);
app.use("/api/allergiesMedications", AllergiesMedications);

// app.use("*", errRoute);

// app.use(errorHandler);

app.listen(port, () =>
  console.log(`API server is ready on http:localhost:${port}`)
);