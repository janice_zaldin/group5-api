import mysql from 'mysql';

const db = mysql.createConnection({
  host: "localhost",
  user: "group5",
  password: "1030",
  database: "group5EMR",
});

db.connect(function (err) {
  if (err) {
    console.error("error connecting: " + err.stack);
    return;
  }

  console.log("Database connected");
});

export default db;