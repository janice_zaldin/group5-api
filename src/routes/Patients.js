import express from 'express'
import db from "../../database/connection.js";


const router = express.Router();



router.get("/test", (req, res) => {
  res.send("You Have Dialed the Patients Route");
});



// Route to get Patient Details

router.get("/", (req, res) => {
  db.query('SELECT * FROM patients', (error, results, fields) => {
    if (error) throw error;
    return res.status(200).send(results);
  })
});


// route to CREATE a New Patient

router.post('/', async (req, res) => {


  db.query('INSERT INTO patients (  healthCardNum, HCVerCode, donerCode, firstName, lastName, gender, pronouns, birthDate)  VALUES (?,?,?,?,?,?,?,?)', [req.body.healthCardNum, req.body.HCVerCode, req.body.donerCode, req.body.firstName, req.body.lastName, req.body.gender, req.body.pronouns, req.body.birthdate],
    (error, results, fields) => {
      if (error) throw error
    });

// calls back to the just created patient, and uses the healthCardNum to find them
    const users = db.query(`select * from patients where healthCardNum = ${req.body.healthCardNum}`, async (error, results, fields) => {
  if (error) throw error;

  const newID = results[0].id
  
// pushes the new values to the cpUsers table
  db.query('INSERT INTO billing (  streetAddress, unitNumber, city, province, country, postalCode, insuranceProvider, policyNumber)  VALUES (?,?,?,?,?,?,?,?)', [req.body.streetAddress, req.body.unitNumber, req.body.city, req.body.province, req.body.country, req.body.postalCode, req.body.insuranceProvider, req.body.policyNumber],
    (error, results, fields) => {
      if (error) throw error   
    });
  return res.status(200).send(results);
    })
})





// Route to UPDATE details for a specific Patient

router.put('/:id', (req, res) => {
  const { healthCardNum, HCVerCode, donerCode, firstName, lastName, gender, pronouns, birthdate } = req.body;
  db.query(
    `UPDATE patients SET healthCardNum='${healthCardNum}', HCVerCode='${HCVerCode}', donerCode='${donerCode}', firstName='${firstName}', lastName='${lastName}', gender='${gender}', pronouns='${pronouns}', birthdate='${birthdate}'  WHERE id=${req.params.id}`,
    (error, results, fields) => {
      if (error) throw error;

      return res.status(200).send(results);
    }
  );
});

// Route to DELETE patient 

router.delete("/:id", (req, res) => {
  db.query(
    `DELETE FROM patients WHERE id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).json({ message: `Patient ${req.params.id} has been deleted.  With possible prejudice` })
    }
  );
});




export default router;