import express from 'express';
import db from "../../database/connection.js";


const router = express.Router();



router.get("/test", (req, res) => {
  res.send("You Have Dialed the Labratory Results Route");
});



// Route to get Patient Test Results, an display them by newest record first


router.get("/:id", (req, res) => {
  db.query(`SELECT * FROM labResults WHERE id=${req.params.id} ORDER BY updated DESC`, (error, results, fields) =>{
    if (error) throw error;
    return res.status(200).send(results);
  })
});

// Route to ADD Patient Laboratory Test Results and Reports


router.post("/:id", (req, res) => {
  const { testName, testDate, orderedBy, attachment } = req.body;
  db.query(`INSERT INTO labResults (testName='${testName}', testDate='${testDate}', orderedBy='${orderedBy}',attachment='${attachment}' WHERE id=${req.params.id}`,
    (error, results, fields) => {
      if (error) throw error;

      return res.status(200).send(results);
    }
  );
});






export default router;