import express from 'express'
import db from "../../database/connection.js";


const router = express.Router();

router.get("/test", (req, res) => {
  res.send("You Have Dialed the Patient Notes Route");
});

// Route to retreive all Patient Notes

router.get("/:id", (req, res) => {
    db.query(`SELECT * FROM patientNotes WHERE id=${req.params.id} ORDER BY updated DESC `, (error, results, fields) =>{
      if (error) throw error;
      return res.status(200).send(results);
    })
  });


// Adds new notes to Patient File 

  router.post("/:id", (req, res) => {
    db.query(`INSERT INTO patientNotes ( patientNote)  VALUES (?,?,?,?) WHERE id=${req.params.id}`, [req.body.type, req.body.imageDate, req.body.image],
      (error, results, fields) => {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });

  export default router;