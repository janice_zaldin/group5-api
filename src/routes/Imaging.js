import express, { request } from 'express'
import db from "../../database/connection.js";


const router = express.Router();


router.get("/test", (req, res) => {
  res.send("You Have Dialed the Imaging Route");
});



// Route to get Patient Images


router.get("/:id", (req, res) => {
  db.query(`SELECT * FROM patientImaging WHERE id=${req.params.id} ORDER BY updated DESC`, (error, results, fields) =>{
    if (error) throw error;
    return res.status(200).send(results);
  })
});


// Route to ADD Patient Images

// will eventually grab data from a dropdown menu on front-end to chose image type (MRI, XRay, UltraSound or CTScan)  

router.post("/:id", (req, res) => {
  db.query(`INSERT INTO patientImaging ( imageType, imageDate, image)  VALUES (?,?,?) WHERE id=${req.params.id}`, [req.body.type, req.body.imageDate, req.body.image],
    (error, results, fields) => {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});



export default router;