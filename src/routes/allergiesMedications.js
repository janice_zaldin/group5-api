import express from 'express'
import db from "../../database/connection.js";


const router = express.Router();



router.get("/test", (req, res) => {
  res.send("You Have Dialed the Allergy & Medications Route");
});


// Route to get specific Patient Allergies and Medications


router.get("/:id", (req, res) => {
  db.query(`SELECT * FROM allMeds WHERE id=${req.params.id} ORDER BY updated DESC `, (error, results, fields) =>{
    if (error) throw error;
    return res.status(200).send(results);
  })
});



// Route to ADD Patient Allergies and Medications

// eventually will speak to a dropdown menu on front-end, to chose allery severity (I-IV), include name of allergy, name of medications, perscribing doctor, date perscribed, and duration of perscription (this can be null value, as there will be a checkbox on the front end for if medication is ongoing, which sould return a boolen value)


router.post("/:id", (req, res) => {
  const {  medName, persDR, startDate, endDate, onGoing, allType, severity } = req.body;
  db.query(`INSERT INTO allMeds (medName='${medName}', persDR='${persDR}', startDate='${startDate}, endDate='${endDate}, ongoing='${onGoing} allType='${allType}', severity='${severity} WHERE id=${req.params.id}`,
  (error, results, fields) => {
    if (error) throw error;

      return res.status(200).send(results);
    }
  );
});




export default router;