import express from 'express'
import db from "../../database/connection.js";


const router = express.Router();



router.get("/test", (req, res) => {
  res.send("You Have Dialed the CRUD Patients Route");
});




// Route to get Billing Details

router.get("/", (req, res) => {
  db.query('SELECT * from billing', (error, results, fields) =>{
    if (error) throw error;
    return res.status(200).send(results);
  })
});


// // route to CREATE a New Patient Billing Information

// router.post('/', (req, res) => {

//   db.query('INSERT INTO billing (  streetAddress, unitNumber, city, province, country, postalCode, insuranceProvider, policyNumber)  VALUES (?,?,?,?,?,?,?,?)', [req.body.streetAddress, req.body.unitNumber, req.body.city, req.body.province, req.body.country, req.body.postalCode, req.body.insuranceProvider, req.body.policyNumber],
//     (error, results, fields) => {
//       if (error) throw error
    
//       return res.status(200).send(results);
//     }
//   );
// });


// Route to UPDATE Billing details for a specific Patient

router.put('/:id', (req, res) => {
  const { healthCard, verCode, donerCode, fName, lName, gender, pronouns, bDate } = req.body;
  db.query(
    `UPDATE billing SET streetAddress='${streetAddress}', unitNumber='${unitNumber}', city='${city}', province='${province}', country='${country}', postalCode='${postalCode}', insuranceProvider='${insuranceProvider}', policyNumber='${policyNumber}'  WHERE id=${req.params.id}`,
     (error, results, fields) => {
      if (error) throw error;

      return res.status(200).send(results);
    }
  );
});


// Route to DELETE patient billing details

router.delete("/:id", (req, res) => {
  db.query(
    `DELETE FROM billing WHERE id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).json()
    }
  );
});

export default router;