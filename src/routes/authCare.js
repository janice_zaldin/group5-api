import express from 'express'
import jsonwebtoken from 'jsonwebtoken'
import argon2 from 'argon2'
import db from "../../database/connection.js";



const router = express.Router();



router.get("/test", (req, res) => {
  res.send("You Have Dialed the Care Providers Authorization Route");
});




// Logs in a registered Care Provider, and creates a JWT

router.post('/', async (req, res) => {

  const reqUser = req.body.userName
  const userPassword = req.body.userPassword

  try {

    const users = db.query('SELECT * FROM cpUsers', async function (error, results, fields) {

      if (error) throw error;
      const userFound = results.find(({ userName }) => userName === reqUser)


      if (userFound) {
        const verified = await argon2.verify(userFound.userPassword, userPassword)

        if (verified) {
          const token = jsonwebtoken.sign({ reqUser }, 'somethingSecretSomethingSafe', { expiresIn: '50m' })

          return res.json({ token })

        }
      }
      return res.status(401).json({ message: "incorrect credentials provided" })
    })

  } catch (err) {
    console.error(err)
}
});





export default router