import express from 'express'
import argon2 from 'argon2'
import db from "../../database/connection.js";
import cors from "cors";

const app = express();
app.use(cors());

const router = express.Router();




router.get("/test", (req, res) => {
  res.send("You Have Dialed the CRUD Care Providers Route");
});




// route to create new Care Provider

router.post('/', async (req, res) => {

  db.query('INSERT INTO careProviders ( firstName, lastName, pronouns, SINum, birthdate, dateHired, dateTerminated, email, accountNumber, transitNumber, branchNumber) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [req.body.firstName, req.body.lastName, req.body.pronouns, req.body.SINum, req.body.birthdate, req.body.dateHired, req.body.dateTerminated, req.body.email, req.body.accountNumber, req.body.transitNumber, req.body.branchNumber],
    (error, results, fields) => {
      if (error) throw error
    });

  // calls back to the just created careprovider, and uses the SIN to find them, hashes the entered password, and asigns it and the username to the CarePrivider
  const users = db.query(`SELECT * FROM careProviders WHERE SINum = ${req.body.SINum}`, async (error, results, fields) => {
    if (error) throw error;

    const newID = results[0].employeeId

    const hash = await argon2.hash(req.body.userPassword)

    // pushes the new values to the cpUsers table
    db.query('INSERT INTO cpUsers ( userName, userPassword, userId )  VALUES (?,?,?)', [req.body.userName, hash, newID],
      (error, results, fields) => {
        if (error) throw error
      });
    return res.status(200).send(results);
  })
});



// Route to UPDATE details for a specific Care Provider


router.put('/:id', (req, res) => {
  const { employeeId, firstName, lastName, pronouns, SIN, birthdate, dateHired, dateTerminated, email, accountNumber, transitNumber, branchNumber } = req.body;
  db.query(
    `UPDATE careProviders SET employeeId='${employeeId}', firstName='${firstName}', lastName='${lastName}', pronouns='${pronouns}', SIN='${SIN}', birthdate='${birthdate}', dateHired='${dateHired}', dateTerminated='${dateTerminated}', email='${email}', accountNumber='${accountNumber}', transitNumber='${transitNumber}', branchNumber='${branchNumber}'  WHERE id=${req.params.id}`,
    (error, results, fields) => {
      if (error) throw error;

      return res.status(200).send(results);
    }
  );
});




// Route to DELETE a care provider 

router.delete("/:id", (req, res) => {
  db.query(
    `DELETE FROM careProviders WHERE id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).json({ message: `Care Provider ${req.params.id} has been deleted.  With possible prejudice` })
    }
  );
});


export default router;