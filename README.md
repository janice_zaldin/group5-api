# SF 1030 Group Project

##### Course Final Project

To begin, make sure you have cloned all three repos: Group5_frontend, Group5_api and Group5_database.



This EMR application contains a RESTful API, Reactjs application, and mySQL database.  Functionality for this application features:

- Allows access to authorized users only
- Validates Care Providers and Administrators for specific functionality.  Admin Users are able to access functionality to create new patients and staff members, update their records, delete those records.
- Care Providers are able to search for specific patients, add records to their files for Imaging, Allergies, Medications and Laboratory Testing.
- Care Providers will also be able to make notations on patient files, and view records of previous annotations made on the patient record.  All records will be displayed in reverse order, with newest files and notes on top.

 The backend API includes data validation and error handling, as well as the appropriate queries to interact with records stored in the dedicated mySQL tables.  JWT Tokens has been used for authentication, while Argon2 has been used for password hashing.  


This project has been initialized on a Linux machine, but should be compatible with Windows or Mac.



## Setup

1. Clone all three repositories, and store them locally.

2. Using the terminal in VScode (or other terminal of your choice), navigate into the folder for the frontend repository (group5_frontend).  Using the terminal, install project dependencies with:

    ```
    npm install

    ``` 

When it is finished, do the same in the backend folder (group5_api).


3. In the server terminal, you can set the environment port with the following command:

For Windows:

    $env:PORT = 1234 //example port number

For Linux or Mac:

    export PORT=1234 //example port number

Note that the server is meant to be running on port 3007.


4. Now, open the document group5-info.sql, contained in the group5_database repository folder.  Using mySQL workbench, or the terminal, execute the first block of queries, one by one, in the order listed.  These queries create the appropriate database, user, password and privileges needed for this project.  Then execute the remaining queries to create the tables, and populate them as appropriate.


5. Now start the Web application by typing in the client (frontend) terminal: 

    ```
    npm start 

    ``` 

   *// (this will start the react app)*

6. Start the server by typing in the server terminal: 

    ```
    npm start 

    ``` 
*// (this will start the server using node)*

or

    ```
    npm run dev 

    ``` 

   *// (this will start the server using nodemon)*
    
Verify your server is running, and that it is running on the correct port.



# SF 1030 Group Project

##### Planned Future Functionality


In future:

 Each of the patient record pages (Imaging, Laboratory Tests, Allergies & Medications and Notes) will not only have the appropriate ability to input new information and documentation into the patient's record, but will also display a history of all activity on that patient's record in that category, sorted with newest entries first. 

A new page will be added, on that lists all information in the patient record, sorted by category, on a single page, ready to print or share as an electronic record.

Due to the private nature of these stored records, database files will be encrypted, possibly using Aargon2, or other appropriate algorithm.

Patient and staff records will not be deleted, but will be flagged for deletion, and stored on a 'inactive files' database table, in case of later audits, and to preserve the integrity of the patient's record.